angular.module('picklemans').service('SandwichApi', function($http, $q, StringUtils, SandwichFactory, CONFIG) {
    var SandwichApi = {};

    SandwichApi.PATHS = {
        GET_ALL: '/sandwiches',
        GET_ONE: '/sandwiches/{id}',
        POST: '/sandwiches'
    };

    SandwichApi.save = function(sandwich) {
        var url = CONFIG.API_HOST + SandwichApi.PATHS.POST;

        return $http({
            method: 'POST',
            url: url,
            data: sandwich
        });
    };

    SandwichApi.fetch = function(id) {
        var deferred = $q.defer(),
            url = CONFIG.API_HOST;

        if(id) {
            url += StringUtils.format(SandwichApi.PATHS.GET_ONE, {
                id: id
            });
        } else {
            url += SandwichApi.PATHS.GET_ALL;
        }

        $http.get(url)
            .then(function(response) {
                SandwichFactory.resetNumber();

                var sandwiches = response.data.map(function(datum) {
                    return SandwichFactory.newSandwich(datum);
                });

                deferred.resolve(sandwiches);
            })
            .catch(function(err) {
                deferred.reject(err);
            });

        return deferred.promise;
    };

    return SandwichApi;
});