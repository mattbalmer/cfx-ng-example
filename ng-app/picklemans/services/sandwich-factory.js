angular.module('picklemans').factory('SandwichFactory', function() {
    var SandwichFactory = {};

    var currentSandwichNumber = 0;

    SandwichFactory.nextNumber = function() {
        return ++currentSandwichNumber;
    };
    SandwichFactory.resetNumber = function() {
        currentSandwichNumber = 0;
    };

    SandwichFactory.newSandwich = function(data) {
        var sandwich = _.clone(data);

        sandwich.number = SandwichFactory.nextNumber();

        return sandwich;
    };

    return SandwichFactory;
});