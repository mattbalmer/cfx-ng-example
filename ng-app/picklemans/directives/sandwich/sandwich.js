angular.module('picklemans').directive('sandwich', function() {
    return {
        templateUrl: '/html/picklemans/directives/sandwich/sandwich.html',
        scope: {
            sandwich: '='
        }
    }
});