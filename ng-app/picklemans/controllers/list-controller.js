angular.module('picklemans').controller('ListController', function($scope, SandwichApi, $location) {
    $scope.sandwiches = [];

    $scope.newSandwich = function() {
        $location.path('/new-sandwich');
    };

    SandwichApi.fetch()
        .then(function(sandwiches) {
            $scope.sandwiches = sandwiches;
        });
});