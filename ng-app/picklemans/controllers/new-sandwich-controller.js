angular.module('picklemans').controller('NewSandwichController', function($scope, SandwichApi, $location) {
    $scope.sandwich = {};

    $scope.showList = function() {
        $location.path('/');
    };

    $scope.submit = function() {
        SandwichApi.save($scope.sandwich)
            .then(function() {
                $location.path('/');
            });
    };
});