angular.module('picklemans')
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'ListController',
                templateUrl: '/html/picklemans/views/list.html'
            })
            .when('/new-sandwich', {
                controller: 'NewSandwichController',
                templateUrl: '/html/picklemans/views/new-sandwich.html'
            })
            .otherwise({ redirectTo: '/' });
    });