module.exports = function (grunt) {
    grunt.initConfig({
        ng_modules: {
            options: {
                minify: false,
                cssDir: 'css',
                jsDir: 'js'
            },
            local: {
                src: 'ng-app',
                dest: 'public'
            }
        },
        watch: {
            files: [ 'ng-app/**/*' ],
            tasks: [ 'ng_modules' ]
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ng-modules');

    grunt.registerTask('compile', ['ng_modules']);
    grunt.registerTask('default', ['ng_modules', 'watch']);
};