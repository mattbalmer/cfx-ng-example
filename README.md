cfx-ng-example
=====

Example project for Carfax AngularJS

Run
=====

Any http server will do, it just needs to serve /public/ as root.

Node http-server:

* http-server public -p 3000

Python simple:

* $ pushd public; python -m SimpleHTTPServer; popd
