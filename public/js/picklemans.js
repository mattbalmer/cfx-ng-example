angular.module('picklemans', ['ngRoute', 'mbStrings'])
    .constant('CONFIG', {
        API_HOST: 'http://cfx-ng-example-server.herokuapp.com/1.0'
    });
angular.module('picklemans').controller('ListController', function($scope, SandwichApi, $location) {
    $scope.sandwiches = [];

    $scope.newSandwich = function() {
        $location.path('/new-sandwich');
    };

    SandwichApi.fetch()
        .then(function(sandwiches) {
            $scope.sandwiches = sandwiches;
        });
});
angular.module('picklemans').controller('NewSandwichController', function($scope, SandwichApi, $location) {
    $scope.sandwich = {};

    $scope.showList = function() {
        $location.path('/');
    };

    $scope.submit = function() {
        SandwichApi.save($scope.sandwich)
            .then(function() {
                $location.path('/');
            });
    };
});
angular.module('picklemans').directive('sandwich', function() {
    return {
        templateUrl: '/html/picklemans/directives/sandwich/sandwich.html',
        scope: {
            sandwich: '='
        }
    }
});
angular.module('picklemans')
    .config(function($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'ListController',
                templateUrl: '/html/picklemans/views/list.html'
            })
            .when('/new-sandwich', {
                controller: 'NewSandwichController',
                templateUrl: '/html/picklemans/views/new-sandwich.html'
            })
            .otherwise({ redirectTo: '/' });
    });
angular.module('picklemans').service('SandwichApi', function($http, $q, StringUtils, SandwichFactory, CONFIG) {
    var SandwichApi = {};

    SandwichApi.PATHS = {
        GET_ALL: '/sandwiches',
        GET_ONE: '/sandwiches/{id}',
        POST: '/sandwiches'
    };

    SandwichApi.save = function(sandwich) {
        var url = CONFIG.API_HOST + SandwichApi.PATHS.POST;

        return $http({
            method: 'POST',
            url: url,
            data: sandwich
        });
    };

    SandwichApi.fetch = function(id) {
        var deferred = $q.defer(),
            url = CONFIG.API_HOST;

        if(id) {
            url += StringUtils.format(SandwichApi.PATHS.GET_ONE, {
                id: id
            });
        } else {
            url += SandwichApi.PATHS.GET_ALL;
        }

        $http.get(url)
            .then(function(response) {
                SandwichFactory.resetNumber();

                var sandwiches = response.data.map(function(datum) {
                    return SandwichFactory.newSandwich(datum);
                });

                deferred.resolve(sandwiches);
            })
            .catch(function(err) {
                deferred.reject(err);
            });

        return deferred.promise;
    };

    return SandwichApi;
});
angular.module('picklemans').factory('SandwichFactory', function() {
    var SandwichFactory = {};

    var currentSandwichNumber = 0;

    SandwichFactory.nextNumber = function() {
        return ++currentSandwichNumber;
    };
    SandwichFactory.resetNumber = function() {
        currentSandwichNumber = 0;
    };

    SandwichFactory.newSandwich = function(data) {
        var sandwich = _.clone(data);

        sandwich.number = SandwichFactory.nextNumber();

        return sandwich;
    };

    return SandwichFactory;
});